# Hotel Reservations
This application is used to add the new reservations and also getting the existing reservations details in the hotel. 

# Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

## Prerequisites
Software needs to be installed to run the application. <br>
1. [Node](https://nodejs.org/)
2. [VSCode](https://code.visualstudio.com/download) or any IDE <br>
3. Android Emulator / iOS Simulator <br>

## Installation
Create an application using below script to generate the boilerplate. <br>
```bash
 react-native init –-template typescript 
```

Clone the project using below script in the terminal
```bash
git clone https://gitlab.com/venkyy_gitlab/hotel-reservation.git
cd hotel-reservation
```

To run the application, need to install npm 
```bash
 npm install 
```

To run the application in specific platform (Android or iOS) need to start Android emulator or iOS simulator or physical device and run the below command in the terminal.<br>
```bash
 react-native run-ios or react-native run-android
```

Running the tests
To run the automated tests use the below script
```bash
 npm run test                                             
```

## Architecture

- Hotel Reservations
    - node_modules 
    - Android - Contains Android native app code
    - iOS - Contains iOS native app code
    - src - Contains source code 
        - api - Contains GraphQL queries and mutations. 
        - components - Contains stateless, lean components.
        - navigation – Contains all the routes. 
        - screens - Contains screens of the application.  
        - App.tsx 
    - index.js 
    - package.json - lists the packages your project depends on.
    - _tests_ - Contains test files for the screens
    - README.md

## Authors
  *Venkatesh Ventrapragada*

