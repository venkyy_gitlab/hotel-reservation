import React from 'react';
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import {
  StyleSheet,
  View,
} from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';

import Routes from './src/navigation/AppNavigator';
//Theming React Native Paper
const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#323e47',
    accent: '#e2000e',
  },
  fonts: {
    regular: 'Roboto',
    medium: 'Roboto',
    light: 'Roboto Light',
    thin: 'Roboto Thin',
  },
};

const client = new ApolloClient({
  link: new HttpLink({
    uri: 'https://us1.prisma.sh/public-luckox-377/reservation-graphql-backend/dev',
  }),
  cache: new InMemoryCache()
});

interface IProps {
}

interface IState {
}

class App extends React.Component<IProps, IState> {
  public render() {
    return (
      <PaperProvider theme={theme}>
      <ApolloProvider client={client}>
        <View style={styles.container}>
          <Routes/>
        </View>
      </ApolloProvider>
      </PaperProvider>
    );
  };
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
});



export default App;
