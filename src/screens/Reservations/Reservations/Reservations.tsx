import React from "react";
import { Query } from "react-apollo";
import { View, ScrollView } from "react-native";
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';
import {
  ActivityIndicator, Text
} from 'react-native-paper';
import ReservationList from "../../../components/ReservationList";
import { RerservationListQueries } from "../../../api";
import styles from "./styles";
import ActionButton from '../../../components/ActionButton'

type Navigation = NavigationScreenProp<NavigationState, NavigationParams>;

interface Props {
  navigation: Navigation;
}
interface Data {
  name: string;
  hotelName: string;
  arrivalDate: Date;
  departureDate: Date;
};

export default class Reservations extends React.PureComponent<Props> {
  public static navigationOptions = ({
    navigation,
  }: {
    navigation: Navigation;
  }) => ({
    title: 'Hilton Reservations',
  });
  public render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Query query={RerservationListQueries} pollInterval={500}>
          {({ data, loading, error }) => {
            if (loading) {
              return (loading) && (
              <ActivityIndicator animating size="large"/>
              )
            }
            if (error) {
              return <Text>Something went wrong! {error}</Text>;
            }
            return (
              <View>
              <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                <View style={{ flex: 1 }} >
                  <ReservationList reservations={data.reservations} navigation={navigation} />
                </View >
              </ScrollView >
              <ActionButton navigation={navigation} />
              </View>
            );
          }}
        </Query>
      </View>
    );
  }
}



