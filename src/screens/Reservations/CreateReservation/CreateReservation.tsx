import React from "react";
import { Text, View } from "react-native";
import ReservationForm from "../../../components/ReservationForm";

import styles from "./styles";

export default class ReservationCreate extends React.PureComponent {
  public static navigationOptions = {
    title: "Create Reservation"
  };

  public render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Fill all the details below</Text>
        <ReservationForm />
      </View>
    );
  }
}
