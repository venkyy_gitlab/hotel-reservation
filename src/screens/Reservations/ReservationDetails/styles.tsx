import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ecf0f1',
  },
  text: {
      fontSize: 22,
      textAlign: 'center',
  },
  wrapper: {
      flex: 1,
  },
  inputContainerStyle: {
      margin: 8,
  },
  button: {
      margin: 4,
  },
  updateByUser: {
      alignItems: 'flex-start'
  },
  btnRow: {
      flexDirection: 'row',
      justifyContent: 'center',
      paddingVertical: 8,
      paddingHorizontal: 16,
      fontSize: 25,
  },
  row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: 8,
      paddingHorizontal: 16,
      fontSize: 25,
  },
  column: {
      flexDirection: 'column',
      // alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: 8,
      paddingHorizontal: 16,
      fontSize: 25,
  },
});