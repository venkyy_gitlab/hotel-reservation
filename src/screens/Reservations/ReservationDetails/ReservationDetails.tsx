import React from 'react';
import {
    View, SafeAreaView,
    KeyboardAvoidingView,
    ActivityIndicator,
} from 'react-native';
//Import UI components from Paper
import {
    Text, Button, Appbar,
    Paragraph, DataTable
} from 'react-native-paper';
// Helper function to fix the Android back button issue
import { AndroidBackHandler } from 'react-navigation-backhandler';
//Injecting navigation props
import { NavigationInjectedProps } from "react-navigation";
//Import component styles 
import styles from './styles';

//Declaring Reservation model
export interface IReservation {
    name: string;
    hotelName: string;
    arrivalDate: string;
    departureDate: string;
}

interface Props extends NavigationInjectedProps {
    item: IReservation;
}

class ReservationDetails extends React.PureComponent<Props> {
    //using Appbar from Paper UI component instead of setting it here
    static navigationOptions = {
        header: null
    };
    onBackButtonPressAndroid = () => {
        /*
        *   Returning `true` from `onBackButtonPressAndroid` denotes that we have handled the event,
        *   and react-navigation's lister will not get called, thus not popping the screen.
        *
        *   Returning `false` will cause the event to bubble up and react-navigation's listener will pop the screen.
        * */
        //Navigate back to homepage
        this.props.navigation.navigate("Reservations")
        return false;
    };

    render() {
        //Getting route params from the List item instead of querying from the server
        const name = this.props.navigation.getParam('name', 'NO-name');
        const hotelName = this.props.navigation.getParam('hotelName', 'NO-hotelName');
        const arrivalDate = this.props.navigation.getParam('arrivalDate', 'NO-arrivalDate');
        const departureDate = this.props.navigation.getParam('departureDate', 'NO-departureDate');
        return (
            <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>
                <SafeAreaView style={styles.wrapper}>
                    <Appbar.Header>
                        <Appbar.BackAction
                            onPress={() => this.props.navigation.navigate("Reservations")}
                        />
                        <Appbar.Content
                            title={"Reservation " + name}
                        />
                    </Appbar.Header>
                    <KeyboardAvoidingView
                        style={styles.wrapper}
                        behavior="padding"
                        keyboardVerticalOffset={10}
                    >
                        <View style={styles.inputContainerStyle}>
                        <Paragraph>Reservation Details</Paragraph>
                            <DataTable.Header>
                                <DataTable.Title>Guest Name</DataTable.Title>
                                <DataTable.Title>Hotel Name</DataTable.Title>
                                <DataTable.Title>Check In</DataTable.Title>
                                <DataTable.Title>Check Out</DataTable.Title>
                            </DataTable.Header>

                            <DataTable.Row>
                                <DataTable.Cell>{name}</DataTable.Cell>
                                <DataTable.Cell>{hotelName}</DataTable.Cell>
                                <DataTable.Cell>{arrivalDate}</DataTable.Cell>
                                <DataTable.Cell>{departureDate}</DataTable.Cell>
                            </DataTable.Row>
                        </View>
                        <View style={styles.btnRow}>
                            <Button color="#323e47" mode="contained" onPress={this.onBackButtonPressAndroid} style={styles.button}>
                                Back
                    </Button>

                        </View>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </AndroidBackHandler>
        );
    }
}

export default ReservationDetails;
