import React from "react";
import { View, Text, TouchableOpacity } from 'react-native';
import { NavigationInjectedProps, withNavigation } from "react-navigation";
import { List } from 'react-native-paper';
import styles from './styles';

export interface IReservation {
  name: string;
  hotelName: string;
  arrivalDate: string;
  departureDate: string;
}


interface Props extends NavigationInjectedProps {
  item: IReservation;
}

class ReservationItem extends React.PureComponent<Props> {
  public render() {
    const { item } = this.props;
    return (
      <View>
        <List.Section style={styles.column}>
          <List.Item style={styles.row} title={item.hotelName}
            left={() => <List.Icon color="#323e47" icon="hotel" />} />
          <List.Item title={item.name}   
            left={() => <List.Icon color="#323e47" icon="person" />} />
          <List.Item title={"Check-In "+item.arrivalDate} />
          <List.Item title={"Check-Out "+item.departureDate} />
        </List.Section>
      </View>
    );
  }
}

export default withNavigation(ReservationItem);
