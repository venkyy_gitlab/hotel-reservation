import React from "react";
import { FlatList, View, TouchableNativeFeedback } from "react-native";
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';

import ReservationItem from "./../ReservationItem";

interface IReservation {
  name: string;
  hotelName: string;
  arrivalDate: string;
  departureDate: string;
}

type Navigation = NavigationScreenProp<NavigationState, NavigationParams>;

interface Props {
  reservations: IReservation[];
  navigation: Navigation;
}

class ReservationList extends React.PureComponent<Props> {
  static navigationOptions = {
    header: null
  };
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

  render() {
    const { reservations, navigation } = this.props;
    return (
      <View>
        <FlatList
          data={reservations}
          keyExtractor={(item: IReservation, index) => index.toString()}
          renderItem={({ item }) => (
            <TouchableNativeFeedback onPress={() => navigation.navigate("ReservationDetails", { 
              'name': item.name,
              'hotelName': item.hotelName,
              'arrivalDate': item.arrivalDate,
              'departureDate': item.departureDate
              })}>
              <View>
                <ReservationItem item={item} />
              </View>
            </TouchableNativeFeedback>
          )}
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>

    );
  };
};

export default ReservationList;
