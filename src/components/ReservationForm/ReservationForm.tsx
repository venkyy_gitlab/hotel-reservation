import React from "react";
import { Formik } from "formik";
import { Mutation } from "react-apollo";
import { View } from "react-native";
import { handleTextInput } from "react-native-formik";
import { withNavigation } from "react-navigation";
import { TextField } from "react-native-material-textfield";
import { Button, Text, Snackbar } from 'react-native-paper';
import * as Yup from "yup";
import { ReservationCreateMutation } from "../../api";

const Input = handleTextInput(TextField);

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Customer name is required"),
  hotelName: Yup.string().required("Hotel name is required"),
  arrivalDate: Yup.date().required("CheckIn date is required"),
  departureDate: Yup.date().required("CheckOut date is required")
});

interface Props {
  navigation: any;
}

interface State {
  visible: Boolean,
  date: Date,
}

class ReservationForm extends React.PureComponent<Props, State> {
  state = {
    visible: false,
    date: new Date(),
  };
  public render() {
    return (
      <Mutation mutation={ReservationCreateMutation}>
        {addReservationMutation => (
          <Formik
            initialValues={{
              name: "",
              hotelName: "",
              arrivalDate: "",
              departureDate: ""
            }}
            onSubmit={values => {
              addReservationMutation({
                variables: {
                  name: values.name,
                  hotelName: values.hotelName,
                  arrivalDate: values.arrivalDate,
                  departureDate: values.departureDate
                }
              })
                .then(res => {
                    <Snackbar
                      visible={this.state.visible}
                      onDismiss={() => this.setState({ visible: false })}
                    >
                      Successfully created Reservation.
                </Snackbar>
                    this.props.navigation.navigate("Reservations");
                })
                .catch((err: any) => <Text>{err}</Text>);
            }}
            validationSchema={validationSchema}
            render={props => {
              return (
                <View>
                  <Input name="name" type="name" label="Name" />
                  <Input name="hotelName" type="hotelName" label="Hotel Name" />
                  <Input
                    name="arrivalDate"
                    type="arrivalDate"
                    label="Arrival Date"
                  />
                  <Input
                    name="departureDate"
                    type="departureDate"
                    label="Depature Date"
                  />
                  <Button
                    mode="contained"
                    onPress={props.handleSubmit}>Submit</Button>
                </View>
              );
            }}
          />
        )}
      </Mutation>
    );
  }
}

export default withNavigation(ReservationForm);
