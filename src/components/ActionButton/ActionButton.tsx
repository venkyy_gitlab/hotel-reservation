import React from "react";
import { View, TouchableOpacity, Platform } from "react-native";
import { Icon, Text, Button } from 'react-native-elements';
import {
    NavigationParams,
    NavigationScreenProp,
    NavigationState,
} from 'react-navigation';

type Navigation = NavigationScreenProp<NavigationState, NavigationParams>;

interface Props {
    navigation: Navigation;
}

export default class ActionButton extends React.PureComponent<Props> {
    public static navigationOptions = ({ navigation }: { navigation: Navigation }) => {
        return {
            headerRight: (
                Platform.OS === 'ios' ? (
                    <Button
                        onPress={() => navigation.navigate("ReservationCreate")}>ADD </Button>) : ""
            )
        };
    };
    public render() {
        const { navigation } = this.props;
        return (
            <View>
                {Platform.OS === 'android' ? (
                    <TouchableOpacity
                        style={{
                            borderWidth: 1,
                            borderColor: 'rgba(0,0,0,0.2)',
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: 70,
                            position: 'absolute',
                            bottom: 10,
                            right: 10,
                            height: 70,
                            backgroundColor: '#fff',
                            borderRadius: 100,
                        }}
                        onPress={() => {
                            navigation.navigate('CreateReservation');
                        }}
                    >
                        <Icon name="add" size={30} color="#01a699" />
                    </TouchableOpacity>
                )
                    : ""}
            </View>
        );
    }
}



