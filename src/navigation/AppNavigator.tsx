import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';

import Reservations from '../screens/Reservations/Reservations';
import CreateReservation from '../screens/Reservations/CreateReservation';
import ReservationDetails from '../screens/Reservations/ReservationDetails';

const RootStack = createStackNavigator(
    {
    CreateReservation,
    Reservations,
    ReservationDetails
    },
    {
      initialRouteName: "Reservations",
      navigationOptions: {
        headerStyle: {
          backgroundColor: "#19A1D5"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold"
        }
      }
    }
  );

  const Routes = createAppContainer(RootStack);

  export default Routes;
